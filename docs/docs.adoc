= Robigalia

Robigalia is a project with two goals:

1.  Build a robust Rust ecosystem around seL4
2.  Create a highly reliable persistent capability OS, continuing the heritage of EROS and Coyotos

We're currently working away at goal 2, as a way to further goal 1. 

To get started with Rust and seL4, first https://robigalia.org/build-environment.html[set up a build environment]. 
From there, the sky's the limit!
We're currently working on a tutorial and some examples while the OS is being created.
In the meantime, https://robigalia.org/CONTRIBUTING.html[maybe you'd like to contribute?]

// Then, check out the https://robigalia.org/tutorial.html[tutorial]
// or https://robigalia.org/examples.html[examples] to see how you can start
// building Rust applications.

Want to know more about Rust, and why it's useful in this context?
https://robigalia.org/why-rust.html[Go here].

Want to know more about microkernels and seL4?
https://robigalia.org/about-microkernels.html[We have that too].

* https://gitlab.com/robigalia/meta/issues[Our project-wide issue tracker]
* https://robigalia.org/contributing.html[Contributing]
* https://robigalia.org/dco.html[Certificate of origin]
* https://robigalia.org/build-environment.html[Setting up a build environment]
* https://robigalia.org/examples.html[Examples]
// * https://robigalia.org/tutorial.html[Tutorial]
